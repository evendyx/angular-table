import { ChangeDetectorRef, Component, Input, OnChanges } from '@angular/core';

import { UserService } from '../../../services/user.service';

@Component({
  selector: "app-user-list",
  templateUrl: "./user-list.component.html"
})
export class UserListComponent implements OnChanges {
  @Input() groupFilters: Object;
  @Input() searchByKeyword: string;
  pcis: any[] = [];
  filteredPCIs: any[] = [];
  constructor(
    private userService: UserService,
    private ref: ChangeDetectorRef
  ) {}
  ngOnInit(): void {
    this.loadPCIs();
  }
  ngOnChanges(): void {
    if (this.groupFilters) this.filterPCIList(this.groupFilters, this.pcis);
  }
  filterPCIList(filters: any, pcis: any): void {
    this.filteredPCIs = this.pcis; //Reset User List
    const keys = Object.keys(filters);
    const filterPCI = pci => {
      let result = keys.map(key => {
        if (!~key.indexOf("pci_p")) {
          if (pci[key]) {
            return String(pci[key])
              .toLowerCase()
              .startsWith(String(filters[key]).toLowerCase());
          } else {
            return false;
          }
        }
      });
      // To Clean Array from undefined if the age is passed so the map will fill the gap with (undefined)
      result = result.filter(it => it !== undefined);
      // Filter the Age out from the other filters
      if (filters["pcito"] && filters["pcifrom"]) {
        if (pci["pci_p"]) {
          if (
            +pci["pci_p"] >= +filters["pcifrom"] &&
            +pci["pci_p"] <= +filters["pcito"]
          ) {
            result.push(true);
          } else {
            result.push(false);
          }
        } else {
          result.push(false);
        }
      }
      return result.reduce((acc, cur: any) => {
        return acc & cur;
      }, 1);
    };
    this.filteredPCIs = this.pcis.filter(filterPCI);
  }
  loadPCIs(): void {
    this.userService.fetchPCIs().subscribe(pcis => (this.pcis = pcis));
    this.filteredPCIs =
      this.filteredPCIs.length > 0 ? this.filteredPCIs : this.pcis;
  }
}
