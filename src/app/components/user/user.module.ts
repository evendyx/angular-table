import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FilterPipe } from '../../pipe/filter.pipe';
import { UserService } from '../../services/user.service';
import { SearchComponent } from '../search/search.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserRoutes } from './user-routing.module';
import { UserComponent } from './user.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, UserRoutes],
  declarations: [UserComponent, SearchComponent, UserListComponent, FilterPipe],
  providers: [UserService]
})
export class UserModule {}
