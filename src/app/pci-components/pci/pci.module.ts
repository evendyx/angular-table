import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FilterPipe } from '../../pipe/filter.pipe';
import { UserService } from '../../services/user.service';
import { PciSearchComponent } from '../pci-search/pci-search.component';
import { PciListComponent } from './pci-list/pci-list.component';
import { PCIRoutes } from './pci-routing.module';
import { PciComponent } from './pci.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, PCIRoutes],
  declarations: [
    PciComponent,
    PciSearchComponent,
    PciListComponent,
    FilterPipe
  ],
  providers: [UserService]
})
export class PciModule {}
