import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PciListComponent } from './pci-list.component';

describe('PciListComponent', () => {
  let component: PciListComponent;
  let fixture: ComponentFixture<PciListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PciListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PciListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
