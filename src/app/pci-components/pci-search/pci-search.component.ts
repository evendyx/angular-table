import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

import { UserService } from '../../services/user.service';

@Component({
  selector: "app-pci-search",
  templateUrl: "./pci-search.component.html",
  styleUrls: ["./pci-search.component.css"]
})
export class PciSearchComponent implements OnInit {
  form: FormGroup;
  pavement_ps = ["CONCRETE (RIGID)", "TANAH", "ASPAL BETON", "ASPAL KERIKIL"];
  @Output() groupFilters: EventEmitter<any> = new EventEmitter<any>();
  searchText: string = "";
  constructor(private fb: FormBuilder, private userService: UserService) {}
  ngOnInit(): void {
    this.buildForm();
  }
  buildForm(): void {
    this.form = this.fb.group({
      ruas_p: new FormControl(""),
      segment_p: new FormControl(""),
      nama_p: new FormControl(""),
      pavement_p: new FormControl(""),
      pcifrom: new FormControl(""),
      pcito: new FormControl("")
    });
  }

  search(filters: any): void {
    Object.keys(filters).forEach(key =>
      filters[key] === "" ? delete filters[key] : key
    );
    this.groupFilters.emit(filters);
  }
}
