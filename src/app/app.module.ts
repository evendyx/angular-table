import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutes } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserModule } from './components/user/user.module';

@NgModule({
  imports: [BrowserModule, AppRoutes, UserModule],
  declarations: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule {}
