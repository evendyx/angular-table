import { Observable, Subject, of } from 'rxjs';

import { Injectable } from '@angular/core';

import { IRI } from '../data/iri.data';
import { PCI } from '../data/user.data';

@Injectable()
export class UserService {
  setGroupFilter$ = new Subject<any>();
  getGroupFilter = this.setGroupFilter$.asObservable();

  constructor() {}

  fetchPCIs(): Observable<any> {
    return of(PCI);
  }
  fetchIRIs(): Observable<any> {
    return of(IRI);
  }
}
