import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FilterPipe } from '../../pipe/filter.pipe';
import { UserService } from '../../services/user.service';
import { IriSearchComponent } from '../iri-search/iri-search.component';
import { IriListComponent } from './iri-list/iri-list.component';
import { IRIRoutes } from './iri-routing.module';
import { IriComponent } from './iri.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, IRIRoutes],
  declarations: [
    IriComponent,
    IriSearchComponent,
    IriListComponent,
    FilterPipe
  ],
  providers: [UserService]
})
export class IriModule {}
